#!/usr/bin/env bash
# Userdata script to be used with aws ec2 instance
# It install the packages necessary to build the latest kernel,
# fetch latest kernel and compile deb packages
#
# I use my kernel config which disables all the spectre / meltdown
# patches. I prefer performance over security for my environment
#

# Install packages required for kernel build
apt update
apt -y install curl bsd-mailx build-essential kernel-package fakeroot libncurses5-dev libssl-dev ccache

# Do the work
BUILDPATH=/my/ssd/kernel-build/
MYKERN=$(uname -r | sed s/-pos//g)
LATESTKERN=$(elinks -dump https://www.kernel.org/  | grep  -A1 Latest | tail -1 | awk -F\] '{print $NF}')
if [ $MYKERN == $LATESTKERN ]; then
	echo already running latest kernel
	exit 0
fi

mkdir -p $BUILDPATH
cd $BUILDPATH
curl -s https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$LATESTKERN.tar.xz | tar Jxf  - -C $BUILDPATH

cd $BUILDPATH/linux-$LATESTKERN
curl -s https://gitlab.com/xpk.headdesk/linux-automation/blob/ef318fce678426c771aa831febd427f8ebae2b9f/kern-config > .config
yes "" | make oldconfig
sudo mount -oremount,exec /tmp
export STARTTIME=$(date +%s)
sudo make -j 8 deb-pkg LOCALVERSION=-pos
export ENDTIME=$(date +%s)

echo "Build info:" > /tmp/kern-auto-build.log
echo "Packages: " >> /tmp/kern-auto-build.log
ls -hg $BUILDPATH/*$LATESTKERN*.deb >> /tmp/kern-auto-build.log
echo "Compile time:  $((ENDTIME - STARTTIME)) seconds" >> /tmp/kern-auto-build.log
uptime >> /tmp/kern-auto-build.log

cat /tmp/kern-auto-build.log | mailx -s "New kernel ready for install - $LATESTKERN" -r kern-auto-build@headdesk.me -- xpk@headdesk.me

# Upload kernel to s3 (work in progress)
# 
